import sbt._

object Modules extends Build {
  lazy val root = RootModule.project

  lazy val sync = Client.project
  lazy val syncApi = ClientApi.project
}
