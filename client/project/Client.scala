import sbt._
import Keys._
import Libraries._
import Modules._
import sbtassembly.Plugin.AssemblyKeys._

object Client extends BaseModule {
  val moduleName = "client"
  val location = "./client"

  val settings = Seq (
    mainClass in assembly := Some("com.client.sync.Main")
  )

  def project = baseProject dependsOn (syncApi)

  lazy val libraries = Seq (typesafeConfig, akkaActor, scalaLogging,
    slf4j, rabbitClient)
}

