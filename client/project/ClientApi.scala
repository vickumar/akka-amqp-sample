import sbt._
import Keys._
import Libraries._

object ClientApi extends BaseModule {
  val moduleName = "client-api"
  val location = "./client-api"

  val settings = Seq ()

  def project = baseProject

  lazy val libraries = Seq (typesafeConfig, scalaXml, scalaParser,
    dispatch, scalaLogging, slf4j, rabbitClient)
}

