object Versions {
  val mine = "1.0"
  val scala = "2.11.1"

  val typesafeConfig = "1.2.1"
  val scalaz = "7.1.0-RC2"
  val logback = "1.0.6"
  val akkaActor = "2.4.1"
  val scalaTest = "2.2.1"
  val mockito = "1.9.5"
}
