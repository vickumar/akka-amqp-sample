import sbt._

object Libraries {

  lazy val scalaz = "org.scalaz" %% "scalaz-core" % Versions.scalaz
  lazy val typesafeConfig = "com.typesafe" % "config" % Versions.typesafeConfig
  lazy val logback = "ch.qos.logback" % "logback-classic" % Versions.logback  
  lazy val akkaActor = "com.typesafe.akka" % "akka-actor_2.11" % Versions.akkaActor
  lazy val scalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0"
  lazy val slf4j = "org.slf4j" % "slf4j-simple" % "1.7.13"

  val rabbitClient = "com.rabbitmq" % "amqp-client" % "3.5.7"
  val scalaXml = "org.scala-lang.modules" %% "scala-xml" % "1.0.2"
  val scalaParser = "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.1"
  val dispatchV = "0.11.1" // change this to appropriate dispatch version
  val dispatch = "net.databinder.dispatch" %% "dispatch-core" % dispatchV

  object test {
    lazy val scalaTest = "org.scalatest" %% "scalatest" % Versions.scalaTest
    lazy val mockito = "org.mockito" % "mockito-all" % Versions.mockito
  }

  lazy val defaultTestingLibs = {
    import test._

    def inTest(module: ModuleID) = module % "test"

    Seq (
      inTest(scalaTest),
      inTest(mockito)
    )
  }

}
