import sbt._
import Keys._
import Modules._

object RootModule {

  lazy val project = Project (
    id = "client",
    base = file("."),
    settings = moduleSettings
  ) aggregate (
    syncApi,
    sync
  )

  val moduleSettings =
    MyDefaults.settings ++ Seq (
      name := "project"
    )
}
