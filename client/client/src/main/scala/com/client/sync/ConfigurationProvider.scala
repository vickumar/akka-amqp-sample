package com.client.sync

trait ConfigurationProvider {
  def config = GlobalConfiguration.config
}
