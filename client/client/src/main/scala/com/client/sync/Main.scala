package com.client.sync

import java.io.File
import java.util.concurrent.TimeUnit
import scala.concurrent.duration.Duration

import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging

import akka.pattern.ask
import akka.util.Timeout

object Main extends LazyLogging {

  implicit val timeout = Timeout(Duration.create(5, TimeUnit.SECONDS))

  val AmqpHost: String = "client.amqp.host"
  val AmqpVHost: String = "client.amqp.vhost"
  val AmqpUsername: String = "client.amqp.user"
  val AmqpPassword: String = "client.amqp.password"
  val AmqpExchange: String = "client.amqp.exchange"

  def main(args: Array[String]) {
    val config: Config = loadConfiguration()

    val u = args(0)
    val m = args.toList.slice(1, args.length).mkString(" ")

    logger.info("*** Sending message... ***")

    val sender = SyncSender.getSender()
    try {
      sender ? "%s: %s".format(u, m)
      sender ! akka.actor.PoisonPill
      SyncSender.processor.terminate()
      System.exit(0)
    }
    catch {
      case t: Throwable => // do nothing
    }
  }

  private def loadConfiguration(): Config = {
    val configFile = new File("config/client.config")
    require(configFile.exists(), s"The argument provided 'config/client.config' refers to a file that doesn't exist.")

    val config = ConfigFactory.parseFile(configFile)
    verifyConfiguration(config)

    GlobalConfiguration.replace(config)

    config
  }

  private def verifyConfiguration(config: Config) {
    verify(config).containsString(AmqpHost)
    verify(config).containsString(AmqpVHost)
    verify(config).containsString(AmqpUsername)
    verify(config).containsString(AmqpPassword)
    verify(config).containsString(AmqpExchange)
  }

  private def verify(config: Config) = new {
    def containsInt(key: String) {
      require(doesNotThrow {
        config.getInt(key)
      }, s"Config at `$key` not found or not an Int")
    }

    def containsString(key: String) {
      require(doesNotThrow {
        config.getString(key)
      }, s"Config at `$key` not found or not a String")
    }

    def containsStringList(key: String) {
      require(doesNotThrow {
        config.getStringList(key)
      }, s"Config at `$key` not found or not a String List")
    }
  }

  private def doesNotThrow(thunk: => Any): Boolean = {
    try {
      val _ = thunk
      true
    } catch {
      case x: Throwable => throw x
    }
  }

}
