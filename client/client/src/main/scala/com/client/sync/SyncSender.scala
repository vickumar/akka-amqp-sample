package com.client.sync

import akka.actor._
import akka.util.Timeout
import com.rabbitmq.client.{QueueingConsumer, Channel, Connection, ConnectionFactory}
import com.typesafe.scalalogging.LazyLogging
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext


object SyncSender extends LazyLogging {
  lazy val config = GlobalConfiguration.config
  private val connection: Connection = getConnection()
  private val exchange: String = config.getString("client.amqp.exchange")

  lazy val processor = ActorSystem("syncClient")

  implicit val ec = ExecutionContext.Implicits.global
  implicit lazy val timeout = Timeout(60 seconds)

  def getConnection(): Connection = {
    val factory = new ConnectionFactory()
    factory.setHost(config.getString("client.amqp.host"))
    factory.setVirtualHost(config.getString("client.amqp.vhost"))
    factory.setPort(config.getInt("client.amqp.port"))
    factory.setUsername(config.getString("client.amqp.user"))
    factory.setPassword(config.getString("client.amqp.password"))
    factory.newConnection()
  }

  def getSender() = {
    val senderChannel= connection.createChannel()
    val sender = processor.actorOf(Props(
      new SyncPublisher(channel = senderChannel, exchange = exchange)))
    sender
  }
}

class SyncPublisher(channel: Channel, exchange: String) extends Actor with LazyLogging {
  def receive = {
    case msg: String => {
      channel.basicPublish(exchange, "", null, msg.getBytes())
      logger.info(msg)
      sender ! true
    }
    case _ => {}
  }
}
