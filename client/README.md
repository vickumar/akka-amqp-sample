Yaposs Sync
===========

    bin/sbt gen-idea
    ## this will generate files for intellij -- if you want them

    bin/sbt eclipse
    ## this will generate files for eclipse -- if you need them.

    bin/sbt "client/run"
    ## this will run the sync processing

    bin/sbt client/assembly

    ## this will build the deployable jar, along with a config, 
    ## it's all you need

