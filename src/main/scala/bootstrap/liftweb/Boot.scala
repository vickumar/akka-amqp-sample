package bootstrap.liftweb

import net.liftweb._
import util._
import Helpers._
import common.{ Full, Empty, Loggable }
import http._
import sitemap._
import Loc._
import code.model._
import code.amqp.ClientListener
import net.liftweb.squerylrecord.RecordTypeMode._
import code.lib.helpers._

class Boot extends Loggable {
  def boot {

    LiftRules.addToPackages("code")
    MySchemaHelper.initSquerylRecordWithInMemoryDB
    logger.info("Running schemify.  Dropping and recreating database.")
    MySchemaHelper.dropAndCreateSchema


    Props.mode match {
      case Props.RunModes.Development => {
        logger.info("RunMode is DEVELOPMENT")

        LiftRules.liftRequest.append({
          case r if (r.path.partPath match { case "console" :: _ => true case _ => false }) => false
        })
      }
      case Props.RunModes.Production => logger.info("RunMode is PRODUCTION")
      case Props.RunModes.Test => logger.info("RunMode is TEST")
      case _ => logger.info("RunMode is PILOT or STAGING")
    }

    val home = Menu("Home") / "index"
    val mySiteMap = SiteMap(home)
    LiftRules.setSiteMap(mySiteMap)

    LiftRules.uriNotFound.prepend(NamedPF("404handler"){
      case (req,failure) =>
        NotFoundAsTemplate(ParsePath(List("404"),"html",false,false))
    })

    // Use jQuery 1.4
    LiftRules.jsArtifacts = net.liftweb.http.js.jquery.JQuery14Artifacts

    //Show the spinny image when an Ajax call starts
    LiftRules.ajaxStart =
      Full(() => LiftRules.jsArtifacts.show("ajax-loader").cmd)

    // Make the spinny image go away when it ends
    LiftRules.ajaxEnd =
      Full(() => LiftRules.jsArtifacts.hide("ajax-loader").cmd)

    // Force the request to be UTF-8
    LiftRules.early.append(_.setCharacterEncoding("UTF-8"))

    // What is the function to test if a user is logged in?
    LiftRules.loggedInTest = Full(() => true)

    // Use HTML5 for rendering
    LiftRules.htmlProperties.default.set((r: Req) =>
      new Html5Properties(r.userAgent))

    //notice fade out (start after x, fade out duration y)
    LiftRules.noticesAutoFadeOut.default.set((notices: NoticeType.Value) => {
      notices match {
        case NoticeType.Notice => {
          //logger.debug("Notice has been detected and fadeout is set ")
          Full((8 seconds, 4 seconds))
        }
        case _ => {
          Empty
        }
      }
    })

    if (Props.mode != Props.RunModes.Test)
      ClientListener.startListening()

    S.addAround(new LoanWrapper {
      override def apply[T](f: => T): T = {
        val resultOrExcept = inTransaction {
          try {
            Right(f)
          } catch {
            case e: LiftFlowOfControlException => Left(e)
          }
        }

        resultOrExcept match {
          case Right(result) => result
          case Left(except) => throw except
        }
      }
    })

    Runtime.getRuntime().addShutdownHook(new Thread {
      override def run() {
        LiftRules.unloadHooks.toList.foreach{ f => tryo { f() }}
      }
    })
  }
}
