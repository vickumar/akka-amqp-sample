package code.lib.helpers
import net.liftweb.util.{Props, Mailer}
import net.liftweb.util.Mailer._

object MyMailer {

  private lazy val serverEmail: String = Props.get("mail.user").openOr("youcarteapp@gmail.com")

  def sendRegistrationEmail(email: String, invite_code: String) = {
    val registerEmail = <html>
      <head>
        <title>Welcome to youCarte!</title>
      </head>
      <body>
        <h1>Welcome to youCarte</h1>
        <h3>Thank you for registering!</h3>
        <h3>
          Please use the following verification code
          to complete your registration<br/><br/>
          Verification Code: {invite_code}<br/><br/>
        </h3>
        <p>***** DO NOT RESPOND TO THIS MESSAGE *****</p>
      </body>
    </html>

    Mailer.sendMail(
      From(serverEmail),
      Subject("Welcome to youCarte!"),
      To(email),
      registerEmail)
  }
}
