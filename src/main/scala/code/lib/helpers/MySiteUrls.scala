package code.lib.helpers

object MySiteUrls {
  lazy val signup = "/user/signup"
  lazy val login = "/user/login"
  lazy val addLocations = "/locations/add"
  lazy val homepage = "/"
}
