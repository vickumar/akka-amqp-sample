package code.lib.helpers

import net.liftweb.http.S
import net.liftweb.http.js.JsCmds._

import scala.xml.NodeSeq

trait RenderMessages {
  protected def renderNotice(msg: String) =
    <div class="alert alert-info" role="alert">
      <strong>{msg}</strong></div>

  protected def renderError(msg: String) =
    <div class="alert alert-warning" role="alert">
    <strong>{msg}</strong></div>

  protected def setError(errId: String, errValue: String = "") = S.error(errId + "_err", errValue)
  protected def setError(errId: String, errValue: NodeSeq) = S.error(errId + "_err", errValue)

  protected def clearErrors(errorList: Map[String, String]) = {
    errorList.foreach {
      case (error_id, error_value) => setError(error_id)
      case _ => Noop
    }
  }

  protected def hasNoErrors(errorList: Map[String,String]): Boolean = {
      errorList.foreach {
        case (error_id, error_value) =>
          if (error_value.isEmpty)
            setError(error_id)
          else
            setError(error_id, renderError(error_value))
        case _ => Noop
      }
      errorList.filter(!_._2.isEmpty).isEmpty
  }
}
