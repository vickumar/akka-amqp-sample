package code.amqp

import akka.actor.{Props => AkkaProps, Actor, ActorSystem, actorRef2Scala}
import akka.util.Timeout
import code.comet.{MessageService, UpdateMessage}
import code.model.{MySchema, Message}
import com.rabbitmq.client.{Channel, QueueingConsumer, Connection, ConnectionFactory}
import net.liftweb.util.Props
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext

import net.liftweb.squerylrecord.RecordTypeMode._

case class StartListening()
case class SendClientMessage(id: String, msg: String)

object ClientListener {
  private val connection: Connection = getConnection()
  private val exchange: String = Props.get("rabbitmq.exchange").openOr("amqp-sample-test")
  lazy val processor = ActorSystem("syncServer")
  implicit val ec = ExecutionContext.Implicits.global
  implicit lazy val timeout = Timeout(60 seconds)

  private def getConnection(): Connection = {
    val factory = new ConnectionFactory()
    factory.setHost(Props.get("rabbitmq.host").openOr("localhost"))
    factory.setVirtualHost(Props.get("rabbitmq.vhost").openOr(""))
    factory.setPort(Props.getInt("rabbitmq.port").openOr(5672))
    factory.setUsername(Props.get("rabbitmq.user").openOr("guest"))
    factory.setPassword(Props.get("rabbitmq.password").openOr("guest"))
    factory.newConnection()
  }

  def processCallback(msg: String) = {
    print("\nRecieved message '%s'\n".format(msg))
    val m = Message.createRecord.message(msg)
    transaction { MySchema.messages.insert(m) }
    MessageService ! UpdateMessage(m)
  }

  def startListening() = {
    val listenChannel = connection.createChannel()
    listenChannel.exchangeDeclare(exchange, "fanout")
    setupListener(listenChannel,listenChannel.queueDeclare().getQueue(), exchange, processCallback)
  }

  private def setupListener(channel: Channel, queueName: String, f: (String) => Any) {
    val listen = processor.actorOf(AkkaProps(new ListeningClient(channel, queueName, f)))
    processor.scheduler.scheduleOnce(1 second, listen, StartListening)
  }

  private def setupListener(channel: Channel, queueName : String, exchange: String, f: (String) => Any) {
    channel.queueBind(queueName, exchange, "")
    setupListener(channel, queueName, f)
  }
}

class ListeningClient(channel: Channel, queue: String, f: (String) => Any) extends Actor {

  def receive = {
    case _ => startReceving
  }

  def startReceving = {

    val consumer = new QueueingConsumer(channel)
    channel.basicConsume(queue, true, consumer)

    while (true) {
      val delivery = consumer.nextDelivery()
      val msg = new String(delivery.getBody())

      context.actorOf(AkkaProps(new Actor {
        def receive = {
          case some: String => f(some)
        }
      })) ! msg
    }
  }
}