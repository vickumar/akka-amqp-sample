package code
package comet

import net.liftweb._
import http._
import actor._
import model._

case class UpdateMessage(msg: Message)

object MessageService extends LiftActor with ListenerManager {
  var messageUpdates = Vector[Message]()

  def createUpdate = messageUpdates

  override def lowPriority = {
    case (update: UpdateMessage) => {
      messageUpdates = messageUpdates :+ update.msg
      sendListenersMessage(update)
    }
    case _ =>
  }
}