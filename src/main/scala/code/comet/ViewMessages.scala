package code
package comet

import net.liftweb._
import http._
import net.liftweb.common.Full
import scala.collection.mutable.ListBuffer
import scala.xml.NodeSeq
import net.liftweb.util.Helpers._
import model._

class ViewMessages extends CometActor with CometListener  {

  def registerWith = MessageService
  private def noMessagesErr = <b>There are no messages.</b>
  override def lifespan = Full (25 seconds)

  def render = {
    val msgList = Message.getMessages()
    "#messagesList" #> msgList.map { m => "#messageItem *" #> m }
  }

  override def lowPriority = {
    case (update: UpdateMessage) => reRender()
    case _ =>
  }
}