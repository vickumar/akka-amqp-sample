package code.model

import net.liftweb._
import common._
import org.squeryl.{Schema, Session}
import squerylrecord.SquerylRecord
import squerylrecord.RecordTypeMode._
import java.sql.{DriverManager,Connection}
import org.squeryl.internals.{DatabaseAdapter => SquerylDBAdapter}
import org.squeryl.adapters.H2Adapter
import net.liftweb.util.Props
import com.jolbox.bonecp.{BoneCP, BoneCPConfig}
import org.h2.jdbcx.JdbcDataSource


object MySchema extends Schema {
  val messages = table[Message]("message")

  on(messages)(m => {
    declare(m.id is (autoIncremented("message_id_seq")))
  })

}


object MySchemaHelper extends Loggable {

  private var dbSettings: DBSettings = new MyH2DBSettings

  def initSquerylRecordWithInMemoryDB {
    initSquerylRecord(new MyH2DBSettings)
  }


  def dropAndCreateSchema {
    transaction {
      try {
        MySchema.printDdl
        MySchema.drop
        MySchema.create
        InitialData.createInitialData
      } catch {
        case e: Throwable =>
          e.printStackTrace()
          throw e;
      }
    }
  }

  private def initSquerylRecord(db: DBSettings) {
    dbSettings = db
    logger.debug("initSquerylRecord with DBSettings: driver="+db.dbDriver+" url="+db.dbUrl+" user="+db.dbUser+" pw="+db.dbPass)
    SquerylRecord.initWithSquerylSession {
      Class.forName(db.dbDriver)
      val session = Session.create(PoolProvider.getPoolConnection(db), db.dbAdapter)
      session.setLogger(statement => Logger("SqlLog:").debug(statement))
      session
    }
  }

  trait DBSettings {
    val dbAdapter: SquerylDBAdapter
    val dbDriver: String = ""
    val dbUrl: String = ""
    val dbUser: String = ""
    val dbPass: String = ""
  }

  class MyH2DBSettings extends DBSettings with Loggable {
    override val dbAdapter = new H2Adapter;
    override val dbDriver = Props.get("h2.db.driver").openOr("org.h2.Driver")
    override val dbUrl = Props.get("h2.db.url").openOr("jdbc:h2:mem:amqp_test;MODE=MYSQL")
    override val dbUser = Props.get("h2.db.user").openOr("amqp_test")
    override val dbPass = Props.get("h2.db.pass").openOr("amqp_test")
    logger.debug("MyH2DBSettings: seting adapter=H2Adapter driver="+dbDriver+" url="+dbUrl+" user="+dbUser+" pw="+dbPass)
  }

  object PoolProvider extends Loggable {
    def getPoolConnection(db: DBSettings) : Connection = {
      if (pool == null) {
        pool=initPool(db)
      }
      pool.getConnection
    }
    private var pool:BoneCP = null

    private def initPool(db: DBSettings):BoneCP = {
      lazy val config = new BoneCPConfig
      try {
        Class.forName(db.dbDriver)
        config.setJdbcUrl(db.dbUrl)
        config.setUsername(db.dbUser)
        config.setPassword(db.dbPass)
        logger.info("BoneCP connection pool is now initialized.")
        new BoneCP(config)
      } catch {
        case e: Exception => {
          logger.error("BoneCP - FAILED to initialize connection pool.")
          throw new java.lang.Exception("BoneCP - FAILED to initialize connection pool."+e.printStackTrace)
        }
      }
    }
  }
}





