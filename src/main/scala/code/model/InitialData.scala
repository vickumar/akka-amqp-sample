package code
package model

object InitialData {

  def createInitialData {
    val msgList = prepareMessage()
    msgList.foreach(MySchema.messages.insert(_))
  }

  def prepareMessage(): List[Message] = {
    List(Message.createRecord.message("A test message"))
  }
}