package code.model

import org.squeryl.annotations.Column
import net.liftweb.record.field._
import net.liftweb.record._
import net.liftweb.squerylrecord.KeyedRecord
import net.liftweb.squerylrecord.RecordTypeMode._


class Message private() extends Record[Message] with KeyedRecord[Long]
{
  def meta = Message

  @Column(name="id")
  override val idField = new LongField(this)

  val message = new StringField(this, 255)
  val created = new DateTimeField(this)
  val updated = new DateTimeField(this)
}

object Message extends Message with MetaRecord[Message] {
  def getMessages(maxMessages: Int = 5): List[String] = {
    val msgs = scala.collection.mutable.ListBuffer.empty[String]
    val dbMessages = from(MySchema.messages)(m =>
      select(m)
      orderBy(m.created desc))
    var msgNum = 0
    for (m <- dbMessages) {
      if (msgNum < maxMessages)
        msgs += m.message.get
      msgNum += 1
    }
    msgs.toList
  }
}