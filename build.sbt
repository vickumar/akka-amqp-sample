name := "akka-amqp"

version := "0.0.1"

organization := "net.liftweb"

scalaVersion := "2.11.7"

resolvers ++= Seq(
  "snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
  "releases"  at "https://oss.sonatype.org/content/repositories/releases"
)

scalacOptions ++= Seq("-deprecation", "-unchecked")

libraryDependencies ++= {
  val liftVersion = "2.6.2"
  val liftEdition = liftVersion.substring(0, 3)
  Seq(
    "net.liftweb"             %% "lift-webkit"                        % liftVersion           % "compile",
    "net.liftweb" %% "lift-webkit"         % liftVersion % "compile->default" withSources(),
    "net.liftweb" %% "lift-squeryl-record" % liftVersion % "compile->default",
    "net.liftweb" %% "lift-mapper"         % liftVersion % "compile->default",
    "net.liftweb" %% "lift-wizard"         % liftVersion % "compile->default",
    "net.liftweb" %% "lift-testkit"        % liftVersion % "compile->default",
    "org.mindrot" % "jbcrypt"              % "0.3m"            % "compile->default",
    "com.jolbox"  % "bonecp"               % "0.7.1.RELEASE"   % "compile->default",
    "com.h2database"     % "h2"            % "1.4.190",
    "net.liftmodules"         %% ("lift-jquery-module_"+liftEdition)  % "2.9"                 % "compile",
    "net.liftmodules" %% "amqp_2.6" % "1.4-SNAPSHOT",
    "org.eclipse.jetty"       %  "jetty-webapp"                       % "9.2.7.v20150116"     % "compile",
    "org.eclipse.jetty"       %  "jetty-plus"                         % "9.2.7.v20150116"     % "container,test", 
    "org.eclipse.jetty.orbit" %  "javax.servlet"                      % "3.0.0.v201112011016" % "container,test" artifacts Artifact("javax.servlet", "jar", "jar"),
    "ch.qos.logback"          %  "logback-classic"                    % "1.0.6",
    "log4j" % "log4j" % "1.2.17",
    "org.specs2"              %% "specs2"                             % "2.3.12"              % "test",
    "com.typesafe.akka" % "akka-actor_2.11" % "2.3.6",
    "com.rabbitmq" % "amqp-client" % "3.5.7"
  )
}

enablePlugins(JettyPlugin)

enablePlugins(JavaAppPackaging)

bashScriptConfigLocation := Some("${app_home}/../conf/jvmopts")

mainClass in Compile := Some("bootstrap.liftweb.Start")
