Akka AMQP Sample 
================

    sbt gen-idea
    ## this will generate files for intellij -- if you want them

    sbt eclipse
    ## this will generate files for eclipse -- if you need them.

    sbt ~jetty:start 
    ## this will run the jetty server 

    sbt ~package 

    ## this will build the deployable jar